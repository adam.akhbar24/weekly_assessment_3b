const person = {
  name: "Kim Tae-yeon",
  age: 18,
  group: "SNSD",
};
const { name, age, group } = person;
console.log(`${name} made her first debut as member of ${group} when she was ${age} years old.`);
