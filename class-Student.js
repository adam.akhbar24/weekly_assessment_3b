class Student {
  constructor() {
    this.nama = "";
    this.age = null;
    this.dateOfBirth = "";
    this.gender = "";
    this.studentID = "";
    this.hobbies = [];
  }
  setNama(nama) {
    this.nama = nama;
  }
  setAge(age) {
    this.age = age;
  }
  setDOB(dob) {
    this.dateOfBirth = dob;
  }
  setGender(gender) {
    this.gender = gender;
  }
  setSID(SID) {
    this.studentID = SID;
  }
  addhoby(hoby) {
    this.hobbies.push(hoby);
  }
  removehoby(hoby) {
    let potong = this.hobbies.indexOf(hoby);
    this.hobbies.splice(potong, 1);
  }
  getData() {
    return this; //`Name : ${this.nama} age:`
  }
}

const student = new Student();
const adam = new Student();
adam.setNama("adam");
adam.setAge(24);
adam.setDOB("20 february 1998");
adam.setGender("male");
adam.setSID(172466698);
adam.addhoby("Baca Manga");
adam.addhoby("Maen Arknights");
adam.addhoby("Gacha(judi nasep)");
console.log("before", adam);
adam.removehoby("gacha(judi nasep)");
console.log("after", adam);
